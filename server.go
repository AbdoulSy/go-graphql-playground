package main

import (
  "encoding/json"
  "fmt"
  "net/http"
  "io/ioutil"
  "github.com/graphql-go/graphql"
  "github.com/graphql-go/handler"
)

type user struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}


var userType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "User",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
		},
	},
)

var data map[string]user

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"user": &graphql.Field{
				Type: userType,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.String,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					idQuery, isOK := p.Args["id"].(string)
					if isOK {
						return data[idQuery], nil
					}
					return nil, nil
				},
			},
		},
	})

var schema, _ = graphql.NewSchema(
	graphql.SchemaConfig{
		Query: queryType,
	},
)


func executeQuery(query string, schema graphql.Schema) *graphql.Result {
	result := graphql.Do(graphql.Params{
		Schema:        schema,
		RequestString: query,
	})
	if len(result.Errors) > 0 {
		fmt.Printf("wrong result, unexpected errors: %v", result.Errors)
	}
	return result
}

//Helper function to import json from file to map
func importJSONDataFromFile(fileName string, result interface{}) (isOK bool) {
	isOK = true
	content, err := ioutil.ReadFile(fileName)
	if err != nil {
		fmt.Print("Error:", err)
		isOK = false
	}
	err = json.Unmarshal(content, result)
	if err != nil {
		isOK = false
		fmt.Print("Error:", err)
	}
	return
}

func main() {

  _ = importJSONDataFromFile("data.json", &data)

  h := handler.New(&handler.Config{
		Schema: &schema,
		Pretty: true,
	})

  // static file server to serve Graphiql in-browser editor
	fs := http.FileServer(http.Dir("static"))

	// serve HTTP
	http.Handle("/vega/graphql", h)
  http.Handle("/", fs)
  http.Handle("/vega", fs)
  http.Handle("/graphql", h)
	http.ListenAndServe(":8080", nil)
}
